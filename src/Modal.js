import React, {Component} from 'react';
import Button from './Button'
import './Modal.scss'

class Modal extends Component {
    constructor(props){
        super(props);
        this.setWrapperRef = this.setWrapperRef.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    render(){
        const {header, closeButton, text, actions} = this.props.modal;
        const clsButton = closeButton && <a onClick={this.props.handleClick} href="#" className="btn-closeModal"></a>;
        const btn = actions.map(button => <Button text={button}/>);
        return (
            <div>
                <div className='modal-container'>
                    <div ref={this.setWrapperRef} className="modal">
                        <div className="modal-header">
                            <p className="title">{header}</p>
                            {clsButton}
                        </div>
                        <div className="modal-body">
                            <p className="text">
                                {text}
                            </p>
                        </div>
                        <div className="modal-footer">
                            {btn}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    setWrapperRef(node) {
        this.wrapperRef = node;
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside(event) {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.props.handleClick();
            console.log("outside");
        }
    }
}

export default Modal
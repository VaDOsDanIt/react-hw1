import React, {Component} from 'react';
import Modal from './Modal'
import Button from './Button'

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpenFirstModal: false,
            isOpenSecondModal: false,

        }
        this.handleClickFirst = this.handleClickFirst.bind(this);
        this.handleClickSecond = this.handleClickSecond.bind(this);
    }

    render() {
        const firstModal = {
            header: "Модалка 1",
            closeButton: true,
            text: "Текст модалки номер 1",
            actions: ["OK", "CANCEL"]
        }
        const secondModal = {
            header: "Модалка 2",
            closeButton: true,
            text: "Текст модалки номер 2",
            actions: ["Отправить", "Отменить"]

        }
        const modalFirst = this.state.isOpenFirstModal && <Modal modal={firstModal} handleClick={this.handleClickFirst}/>;
        const modalSecond = this.state.isOpenSecondModal && <Modal modal={secondModal} handleClick={this.handleClickSecond}/>;


        return (
            <div>
                <Button text="Open first modal" bgc="red" onClick={this.handleClickFirst}/>
                <Button text="Open second modal" bgc="green" onClick={this.handleClickSecond}/>
                {modalFirst}
                {modalSecond}
            </div>
        );
    }

    handleClickFirst() {
        this.setState({
            isOpenFirstModal: !this.state.isOpenFirstModal,
        })
    }
    handleClickSecond() {
        this.setState({
            isOpenSecondModal: !this.state.isOpenSecondModal,
        })
    }

}


export default App;